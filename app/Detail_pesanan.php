<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail_pesanan extends Model
{
    //
    protected $table = 'detail_pesanan';
    protected $primaryKey = 'id_detail_pesanan';
    protected $fillable = ['id_menu',
						   'jumlah',
						   'total_detail',
						   'id_pesanan',
						]; 
}
