<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pesanan extends Model
{
    //
    protected $table = 'pesanan';
    protected $primaryKey = 'id_pesanan';
    protected $fillable = ['no_pesanan',
						   'nama_pesanan',
						   'no_meja',
						   'id_karyawan',
						   'status']; 
}
