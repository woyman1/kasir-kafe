<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu_makanan extends Model
{
    //
    // use Notifiable;

    protected $table = 'menu_makanan';
    protected $primaryKey = 'id_menu';
    protected $fillable = ['nama_menu',
						   'jenis_menu',
						   'harga',
						   'ready'];
}
