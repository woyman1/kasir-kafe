<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wisata extends Model
{
    //

	protected $table = 'wisata';
	protected $primaryKey   = 'id_wisata';
    protected $fillable 	= ['nama_wisata',
    						   'desc',
    						   'alamat',
    						   'kabupaten_kota',
    						   'kecamatan',
    						   'keluranan',
    						   'longitude',
    						   'latitude'
    						];

}
