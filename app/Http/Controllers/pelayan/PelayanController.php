<?php

namespace App\Http\Controllers\pelayan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PelayanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('pelayan.home');
    }

    public function backtoindex()
    {
        return redirect('pelayan/home');
    }
}
