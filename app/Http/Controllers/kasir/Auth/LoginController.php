<?php

namespace App\Http\Controllers\kasir\Auth;
use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'kasir/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:kasir')->except('logout');
    }

    public function showLoginForm() 
    {
        return view('auth.login_kasir');
    }

    protected function guard()
    {
        return Auth::guard('kasir');
    }

    public function username()
    {
        return 'email_karyawan';
    }

    protected function authenticated(Request $request, $user)
    {
        if($user->level_karyawan == 'kasir')
        {
            return redirect('kasir/home');
        }else{
               return redirect('kasir/logout');
        }
    }
     public function logout(Request $request)
    {
        Auth::guard('kasir')->logout();
        return redirect('/');
    }

}
