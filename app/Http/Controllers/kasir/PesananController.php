<?php

namespace App\Http\Controllers\kasir;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pesanan;
use App\Detail_pesanan;
use App\Menu_makanan;
use App\Pembayaran;
use Auth;


class PesananController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:kasir');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        $pesanan = Pesanan::leftJoin('pembayaran', 'pembayaran.id_pesanan', '=', 'pesanan.id_pesanan')
                            ->where('pesanan.status', '=', 'aktif')->get();

        return view('kasir.listPesanan', compact('pesanan'));
    }

    public function backtoindex()
    {
        return redirect('kasir/home');
    }
    public function tambahpesanan()
    {   

        $no = Pesanan::count()+1;
        $id = sprintf("%03s", $no);

        $noPesanan = 'ERP'.date('d').date('m').date('Y').'-'.$id;

        $menu = Menu_makanan::all();

        return view('kasir.tambahpesanan', compact('menu', 'noPesanan'));   
    }

    public function carimenu(Request $request)
    {
        $key = $request->keyword;
        $menu = Menu_makanan::where('nama_menu', 'like', "%$key%")->get();

        echo json_encode($menu->toArray());
    }

    public function getMenu(Request $request)
    {
        $idMenu = $request->id_menu;
        $menu = Menu_makanan::find($idMenu);

        echo json_encode($menu->toArray());
    }

    public function simpanpesanan(Request $request)
    {
        if($request->harga_total_semua == '' || $request->harga_total_semua == 0 )
        {
            return back();
        }

        $pesanan = new Pesanan;

        $pesanan->no_pesanan = $request->no_pesanan;
        $pesanan->nama_pesanan = $request->nama_pemesan;
        $pesanan->no_meja = $request->no_meja;
        $pesanan->id_karyawan = Auth::user()->id_karyawan;
        $pesanan->status = 'aktif';
        $pesanan->created_at = \Carbon\Carbon::now('Asia/Jakarta');  
        $pesanan->save();

        $jml_item = count($request->id_menu_pesanan);

        for($a = 0; $a < $jml_item; $a++)
        {
             $detail = new Detail_pesanan;
             $detail->id_menu = $request->id_menu_pesanan[$a];
             $detail->jumlah = $request->jml_pesanan[$a];
             $detail->total_detail = $request->total_harga_item[$a];
             $detail->id_pesanan = $pesanan->id_pesanan;
             $detail->created_at = \Carbon\Carbon::now('Asia/Jakarta'); 
             $detail->save();
        }

        $pembayaran = new Pembayaran;
        $pembayaran->id_pesanan = $pesanan->id_pesanan;
        $pembayaran->total_harga = $request->harga_total_semua;
        $pembayaran->status_pembayaran = 'pending';
        $pembayaran->created_at = \Carbon\Carbon::now('Asia/Jakarta'); 
        $pembayaran->save();

        return redirect('kasir/daftarpesanan');

    }

    public function hapusPesanan(Request $request)
    {
        $pesanan = Pesanan::find($request->dataId);
        $pesanan->status = 'selesai';

        if($pesanan->save())
        {
            echo '1';
        }else{
            echo '0';
        }

    }

    public function formUpdate($id)
    {
        $data = [];
        $menu = Menu_makanan::all();
        $pesanan = Pesanan::find($id);
        $detail = Detail_pesanan::leftJoin('menu_makanan', 'menu_makanan.id_menu', '=', 'detail_pesanan.id_menu')
                                ->where('detail_pesanan.id_pesanan','=',$id)->get();

        $pembayaran = Pembayaran::where('id_pesanan','=',$pesanan->id_pesanan)->get();

        $data['pesanan']    = $pesanan;
        $data['detail']     = $detail;
        $data['pembayaran'] = $pembayaran[0];

        // echo "<pre>";                            
        // print_r($data['pembayaran']);
        // echo "</pre>";

        return view('kasir.editPesanan', compact('menu', 'data'));
    }

    public function simpanUpdate(Request $request)
    {   
        if($request->harga_total_semua == '' || $request->harga_total_semua == 0 )
        {
            return back();
        }

        $pesanan = Pesanan::find($request->id_pesanan);
        $pesanan->nama_pesanan = $request->nama_pemesan;
        $pesanan->no_meja = $request->no_meja;
        $pesanan->updated_at = \Carbon\Carbon::now('Asia/Jakarta');
        $pesanan->save();

        $jml_item = count($request->id_menu_pesanan);

        for($a = 0; $a < $jml_item; $a++)
        {
            if(isset($request->id_detail[$a]))
            {
                Detail_pesanan::where('id_detail_pesanan','=', $request->id_detail[$a])
                            ->update(['id_menu'         => $request->id_menu_pesanan[$a],
                                      'jumlah'          => $request->jml_pesanan[$a],
                                      'total_detail'    => $request->total_harga_item[$a],
                                      'updated_at'      => \Carbon\Carbon::now('Asia/Jakarta'),
                                  ]);    
            }else{

                 $detail = new Detail_pesanan;
                 $detail->id_menu = $request->id_menu_pesanan[$a];
                 $detail->jumlah = $request->jml_pesanan[$a];
                 $detail->total_detail = $request->total_harga_item[$a];
                 $detail->id_pesanan = $request->id_pesanan;
                 $detail->created_at = \Carbon\Carbon::now('Asia/Jakarta'); 
                 $detail->save();
            }
            
        }

        Pembayaran::where('id_pembayaran','=', $request->id_pembayaran)
                    ->where('id_pesanan','=',$request->id_pesanan)
                    ->update(['total_harga' => $request->harga_total_semua]);

        // echo "<pre>";                            
        // print_r($request->toArray());
        // echo "</pre>";

        return redirect('kasir/daftarpesanan');
    }
    public function laporan()
    {
        $pemesanan = Pesanan::leftJoin('pembayaran','pesanan.id_pesanan','=','pembayaran.id_pesanan')
                        ->where('pesanan.id_karyawan', '=', Auth::user()->id_karyawan)->get();

        return view('kasir.laporanPemesanan', compact('pemesanan'));
    }

}
