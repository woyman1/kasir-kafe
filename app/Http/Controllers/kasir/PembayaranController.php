<?php

namespace App\Http\Controllers\kasir;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pesanan;
use App\Detail_pesanan;
use App\Menu_makanan;
use App\Pembayaran;
use Auth;


class PembayaranController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:kasir');
    }

    public function index()
    {
    	$pembayaran = Pembayaran::leftJoin('pesanan', 'pesanan.id_pesanan','=', 'pembayaran.id_pesanan')
                                ->where('pesanan.status', '=', 'aktif')->get();

        return view('kasir.listPembayaran', compact('pembayaran'));
    }

    public function bayar($id)
    {
        $pembayaran = Pembayaran::leftJoin('pesanan', 'pesanan.id_pesanan','=', 'pembayaran.id_pesanan')
                                ->where('pesanan.status', '=', 'aktif')->get();    

        $detail = Detail_pesanan::leftJoin('menu_makanan', 'detail_pesanan.id_menu', '=', 'menu_makanan.id_menu')
                                ->where('detail_pesanan.id_pesanan','=',$pembayaran[0]->id_pesanan)->get();

                            

        return view('kasir.formPembayaran', compact('pembayaran', 'detail'));
    }

    public function simpanBayar(Request $request)
    {
        $pembayaran = Pembayaran::find($request->id_pembayaran);

        $pembayaran->bayar = $request->tunai;
        $pembayaran->kembali = $request->kembalian;
        $pembayaran->id_kasir = Auth::user()->id_karyawan;
        $pembayaran->status_pembayaran = 'terbayar';
        $pembayaran->updated_at = \Carbon\Carbon::now('Asia/Jakarta'); 

        $pembayaran->save();

        $pesanan = Pesanan::find($pembayaran->id_pesanan);
        $pesanan->status = 'selesai';
        $pesanan->updated_at = \Carbon\Carbon::now('Asia/Jakarta');  
        $pesanan->save();

        return redirect('kasir/listpembayaran');
    }

    public function laporan()
    {
        $pembayaran = Pembayaran::leftJoin('pesanan','pesanan.id_pesanan','=','pembayaran.id_pesanan')
                        ->where('pembayaran.id_kasir', '=', Auth::user()->id_karyawan)->get();

        return view('kasir.laporanPembayaran', compact('pembayaran'));

    }

}