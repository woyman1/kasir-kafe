<?php

namespace App\Http\Controllers\kasir;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class KasirController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:kasir');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('kasir.home');
    }
    public function backtoindex()
    {
        return redirect('kasir/home');
    }
}
