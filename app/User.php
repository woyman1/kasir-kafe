<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'karyawan';
    protected $primaryKey = 'id_karyawan';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
                            'name_karyawan', 
                            'email_karyawan',
                            'password',
                            'level_karyawan',
                            'nohp_karyawan',
                        ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // *
    //  * The attributes that should be cast to native types.
    //  *
     // * @var array
     
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
