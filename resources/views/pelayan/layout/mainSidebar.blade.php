<div class="left-sidenav">
   <ul class="metismenu left-sidenav-menu" id="side-nav">
      <li class="menu-title">Main</li>
      <li>
         <a href="javascript: void(0);"><i class="mdi mdi-email-variant"></i><span>Pesanan</span><span class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
         <ul class="nav-second-level" aria-expanded="false">
            <li><a href="{{ URL::to('pelayan/daftarpesanan') }}">Daftar Pesanan</a></li>
            <li><a href="{{ URL::to('pelayan/tambahpesanan') }}">Tambah Pesanan</a></li>
         </ul>
      </li>
      <li><a href="{{ URL::to('pelayan/laporanpemesanan') }}"><i class="mdi mdi-chart-line"></i><span>Laporan</span></a></li>
      
   </ul>
</div>