<!DOCTYPE html>
<html lang="en">
  @include('pelayan.layout.mainHeader')
  @yield('pageCss')

<body>
   
  @include('pelayan.layout.mainNavbar') 
  
  <div class="page-wrapper">
    @include('pelayan.layout.mainSidebar')
    <div class="page-content">
    
      @yield('content')  
  
      <footer class="footer text-center text-sm-left">&copy; 2018 Amezia <span class="text-muted d-none d-sm-inline-block float-right">Crafted with <i class="mdi mdi-heart text-danger"></i> by Themesbrand</span></footer>

    </div> 
    
    
  </div>
  
  <!-- Uncomment below i you want to use a preloader -->
  <!-- <div id="preloader"></div> -->

  <!-- JavaScript Libraries -->
  @include('pelayan.layout.mainScript') 
  @yield('scriptPage')  

</body>
</html>
