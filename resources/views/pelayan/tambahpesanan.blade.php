@extends('pelayan.layout.mainLayout')

@section('pageCss')
@endsection
@section('content')
	<div class="container-fluid">
       <!-- Page-Title -->
       <div class="row">
          <div class="col-sm-12">
             <div class="page-title-box">
                <div class="float-right">
                   <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="javascript:void(0);">Pelayan</a></li>
                      <li class="breadcrumb-item active">Tambah Pesanan</li>
                   </ol>
                </div>
                <h4 class="page-title">Tambah Pesanan</h4>
             </div>
          </div>
       </div>
       <!-- end page title end breadcrumb -->

       <div class="row">
         <div class="col-md-12">
            <div class="card">
              <div class="card-body">
                
                <div class="row">
                  <div class="col-md-12">
                    <label for="example-text-input" class="col-md-12 col-form-label">Pilih Menu
                        <div class="pull-right">
                           <div class="input-group mb-3">
                                <span class="input-group-prepend">
                                    <button type="button" class="btn btn-light">
                                      <i class="fa fa-search"></i>
                                    </button> 
                                </span>
                                <input type="text" id="cariMenu" name="example-input1-group2" class="form-control input-sm" placeholder="Cari Menu">
                            </div>
                        </div>
                    </label>
                    <table class="table mb-0">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Nama Makanan</th>
                          <th>Jenis Makanan</th>
                          <th>Harga</th>
                          <th>Status</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody id="bodyTableListMenu">
                        @foreach($menu as $m ) 
                        <tr id="row{{ $m->id_menu }}">
                          <th scope="row"></th>
                          <td>{{ $m->nama_menu }}</td>
                          <td>{{ $m->jenis_menu }}</td>
                          <td>{{ $m->harga }}</td>
                          <td>@if($m->ready == 'ready')
                                <span class="badge badge-boxed badge-success">Ready</span>
                              @else
                                <span class="badge badge-boxed badge-danger">Not ready</span>
                              @endif
                          </td>
                          <td> <button class="btn btn-primary btn-sm input-order"  data-id="{{ $m->id_menu }}" <?php echo ( $m->ready != 'ready') ? 'disabled' : ''; ?>>+ Tambahkan</button> </td>
                        </tr>                   
                       
                       @endforeach
                      </tbody>
                    </table>

                  </div>
                </div>

              </div>
            </div>
         </div>
       </div>
      
			<div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-body">
                <h3>Pesanan Baru</h3>
              <form action="{{ URL::to('pelayan/simpanpesanan') }}" method="post">
              <div class="row">
                 
                <div class="col-md-5">
                
                  @csrf
                  <div class="form-group row">
                    <label for="example-text-input" class="col-sm-2 col-form-label">No. Order</label>
                    <div class="col-sm-10">
                      <input class="form-control" type="text" value="{{ $noPesanan }}" name="no_pesanan" readonly required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="example-text-input" class="col-sm-2 col-form-label">Nama Pemesan</label>
                    <div class="col-sm-10">
                      <input class="form-control" type="text" value="" name="nama_pemesan" required>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="example-text-input" class="col-sm-2 col-form-label">No. Meja</label>
                    <div class="col-sm-10">
                      <input class="form-control" type="text" value="" name="no_meja" required>
                    </div>
                  </div>

                </div>
                <div class="col-md-7">
                  <table class="table mb-0">
                    <thead>
                      <tr>                        
                        <th>Nama Makanan</th>                        
                        <th>@</th>
                        <th>Jumlah</th>
                        <th>Total</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody id="bodyTableListOrder">
                        <!-- <tr>
                          <input type="hidden" name="id_menu_pesanan[]" value="">
                          <input type="hidden" name="total_harga_item[]" class="total_harga_item" value=""> 
                          <td></td>
                          <td class="harga_pcs"></td>
                          <td><input type="hidden" value="" name=" []"  style="max-width:100px"  class="jml-item"></td>
                          <td class="total-item"></td>
                          <td></td>
                        </tr>             -->       
                    </tbody>
                    <tbody>
                      <tr>
                        <input type="hidden" id="harga_total_semua" name="harga_total_semua" required>
                        <th colspan="3">Total Semua</th>                                                
                        <th id="harga_total">0</th>
                        <th ><button type="submit" class="btn btn-success">Simpan</button></th>                        
                      </tr>
                    </tbody>
                  </table>
                
                </div>

              </div>                
            </form>
              </div>
            </div>
          </div>
      </div>
		


        

 	</div>
@endsection

@section('scriptPage')
<script type="text/javascript">
  
  $(document).ready(function(){

    $('#cariMenu').keyup(function(event) {
        
        var val = $(this).val();
        var data = getMenuByKeyword(val);

        var menu = JSON.parse(data);
        $('#bodyTableListMenu').html('');
        $.each(menu, function( index, e ) {
          
           if(e.ready == 'ready')
            {
              var ready = '<pan class="badge badge-boxed badge-success">Ready</span>';
              var disabled = '';
            }else{
              var ready = '<span class="badge badge-boxed badge-danger">Not ready</span>';
              var disabled = 'disabled';
            }

            var row = '<tr id="row'+e.id_menu+'"><th scope="row"></th><td>'+e.nama_menu+'</td><td>'+e.jenis_menu+'</td><td>'+e.harga+'</td><td>'+ready+'</td><td> <button class="btn btn-primary btn-sm input-order" data-id="'+e.id_menu+'"  '+disabled+'>+ Tambahkan</button> </td></tr>';

            $('#bodyTableListMenu').append(row);                          

        }); 

         $('.input-order').click(function(event) {
             var id_menu = $(this).attr('data-id');
             // alert(id_menu);
             // $('#row'+id_menu).remove();
             getMenu(id_menu);
         });                   
        
    });

    $('.input-order').click(function(event) {
       var id_menu = $(this).attr('data-id');
       // alert(id_menu);
       // $('#row'+id_menu).remove();       
       getMenu(id_menu);

    });

    $('.deleteOrder').click(function(event) {
      var idrow = $(this).attr('dataid');
      $('#orderRow'+idrow).remove();
    });

    $('body').on('DOMSubtreeModified', "#bodyTableListOrder", function() {
      // alert('changed');
      var total = parseInt('0');
      $(".total-item").each(function() {
        val = parseInt($(this).html());
        total = total+val;        
        
        // alert(parseInt($(this).html()));
      });

      $('#harga_total').html(String(total));
      $('#harga_total_semua').val(total);
      // console.log(total);

    });

    function getMenu(id_menu)
    {
      if($('#orderRow'+id_menu)[0])
      {
        var jmlItem = parseInt($('#orderRow'+id_menu).find('.jml-item').val())+1;

        // alert(jmlItem);

        $('#orderRow'+id_menu).find('.jml-item').val(jmlItem);
        var harga = $('#orderRow'+id_menu).find('.harga_pcs').html();
        $('#orderRow'+id_menu).find('.total-item').html(harga*jmlItem);
        $('#orderRow'+id_menu).find('.total_harga_item').val(harga*jmlItem);

        $('#orderRow'+id_menu).find('.jml-item').keyup(function(e){
          var val =  parseInt($(this).val()); 
          // console.log(val);
          var hargas = parseInt($('#orderRow'+id_menu).find('.harga_pcs').html());
          $('#orderRow'+id_menu).find('.total-item').html(hargas*val);
          $('#orderRow'+id_menu).find('.total_harga_item').val(hargas*val);
        });


      }else{
          
          var data = {};
          $.ajax({
              url: '{{ URL::to("pelayan/carimenu") }}',
              type: 'get',          
              headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
              async: false,
              data: {id_menu: id_menu },
              success: function(m)
                      {                    
                        data = m;
                      }
      
          });

          var menu = JSON.parse(data);

           var rowOrder = '<tr id="orderRow'+menu.id_menu+'"><input type="hidden" name="id_menu_pesanan[]" value="'+menu.id_menu+'"><input type="hidden" name="total_harga_item[]" class="total_harga_item" value="'+menu.harga+'"> <td>'+menu.nama_menu+'</td><td class="harga_pcs">'+menu.harga+'</td><td><input value="1" name="jml_pesanan[]"  style="max-width:100px"  class="jml-item"></td><td class="total-item">'+menu.harga+'</td><td><button type="button" class="btn btn-danger btn-sm deleteOrder" dataid="'+menu.id_menu+'">Hapus</button></td></tr>';

           $('#bodyTableListOrder').append(rowOrder);

      }

      $('.deleteOrder').click(function(event) {
            var idrow = $(this).attr('dataid');
            $('#orderRow'+idrow).remove();
      });

      $('#orderRow'+id_menu).find('.jml-item').keyup(function(e){
          var val =  parseInt($(this).val()); 
          console.log(val);
          var hargas = parseInt($('#orderRow'+id_menu).find('.harga_pcs').html());
          $('#orderRow'+id_menu).find('.total-item').html(hargas*val);
          $('#orderRow'+id_menu).find('.total_harga_item').val(hargas*val);
        });
          
        
    }

    function getMenuByKeyword(val)
    {
      var data = {};
      $.ajax({
          url: '{{ URL::to("pelayan/carimenu") }}',
          type: 'post',
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          data: {keyword: val },
          async: false,
          success: function(m)
                  { 
                      data = m;
                  }
        });
      return data;
    }

  });

</script>
	  
@endsection

