 <head>
      <meta charset="utf-8">
      <title>Kasir</title>
      <meta name="viewport" content="width=device-width,initial-scale=1">
      <meta content="" name="description">
      <meta content="woyman11" name="author">
      <meta name="csrf-token" content="{{csrf_token()}}">
      <!-- App favicon -->
      <link rel="shortcut icon" href="{{ asset('admin/assets/images/favicon.ico') }}">
      <!--Morris Chart CSS -->  
      <!-- <link rel="stylesheet" href="{{ asset('assets/plugins/morris/morris.css') }}"> -->
      <link href="{{ asset('admin/assets/plugins/metro/MetroJs.min.css') }}" rel="stylesheet">
      <!-- App css -->
      <link href="{{ asset('admin/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
      <link href="{{ asset('admin/assets/css/icons.css') }}" rel="stylesheet" type="text/css">
      <link href="{{ asset('admin/assets/css/metismenu.min.css') }}" rel="stylesheet" type="text/css">
      <link href="{{ asset('admin/assets/css/style.css') }}" rel="stylesheet" type="text/css">
</head>