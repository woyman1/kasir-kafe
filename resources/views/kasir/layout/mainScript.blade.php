      <script src="{{ asset('admin/assets/js/jquery.min.js') }}"></script>
      <script src="{{ asset('admin/assets/js/bootstrap.bundle.min.js') }}"></script>
      <script src="{{ asset('admin/assets/js/metisMenu.min.js') }}"></script>
      <script src="{{ asset('admin/assets/js/waves.min.js') }}"></script>
      <script src="{{ asset('admin/assets/js/jquery.slimscroll.min.js') }}"></script>
      <!--Plugins-->
      <!-- <script src="{{ asset('assets/plugins/morris/morris.min.js') }}"></script> -->
      <!-- <script src="{{ asset('assets/plugins/raphael/raphael-min.js') }}"></script> -->
      <!-- <script src="{{ asset('assets/plugins/metro/MetroJs.min.js') }}"></script> -->
      <!-- <script src="{{ asset('assets/plugins/jquery-knob/excanvas.js') }}"></script> -->
      <!-- <script src="{{ asset('assets/plugins/jquery-knob/jquery.knob.min.js') }}"></script> -->
      <!-- <script src="{{ asset('assets/pages/jquery.dashboard.init.js') }}"></script> -->
      <!-- App js -->
      <script src="{{ asset('admin/assets/js/app.js') }}"></script>