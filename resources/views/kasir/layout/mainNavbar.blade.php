<!-- Top Bar Start --> 
      <div class="topbar">
         <!-- LOGO -->
         <div class="topbar-left">
            <a href="index.html" class="logo">
               <span>
                  <a href="#"  class="logo-lg">
                     <h2 class="text-white mt-4">Kasir</h2>
                  </a>                  
               </span>
            </a>
         </div>
         <!-- Navbar -->
         <nav class="navbar-custom">
            <!-- Search input --> 
            <div class="search-wrap" id="search-wrap">
               <div class="search-bar"><input class="search-input" type="search" placeholder="Search here.."> <a href="javascript:void(0);" class="close-search search-btn" data-target="#search-wrap"><i class="mdi mdi-close-circle"></i></a></div>
            </div>
            <ul class="list-unstyled topbar-nav float-right mb-0">            
               <li><a class="nav-link waves-effect waves-light" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                     <i class="mdi mdi-logout-variant nav-icon"></i>
                   </a>
                </li>
                  <!-- logout form -->
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                  </form>

            </ul>
            <ul class="list-unstyled topbar-nav float-right mb-0">
            
               <li class="hidden-sm"><a class="nav-link waves-effect waves-light" href="javascript:void(0);" id="btn-fullscreen"><i class="mdi mdi-fullscreen nav-icon"></i></a></li>
            </ul>
            
            <ul class="list-unstyled topbar-nav mb-0">
               <li><button class="button-menu-mobile nav-link waves-effect waves-light"><i class="mdi mdi-menu nav-icon"></i></button></li>
            </ul>
         </nav>
         <!-- end navbar-->
      </div>