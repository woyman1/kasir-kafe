<div class="left-sidenav">
   <ul class="metismenu left-sidenav-menu" id="side-nav">
      <li class="menu-title">Main</li>
      <li>
         <a href="javascript: void(0);"><i class="mdi mdi-email-variant"></i><span>Pesanan</span><span class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
         <ul class="nav-second-level" aria-expanded="false">
            <li><a href="{{ URL::to('kasir/daftarpesanan') }}">Daftar Pesanan</a></li>
            <li><a href="{{ URL::to('kasir/tambahpesanan') }}">Tambah Pesanan</a></li>
         </ul>
      </li>
      <li><a href="{{ URL::to('kasir/listpembayaran') }}"><i class="mdi mdi-cash-multiple"></i><span>Pembayaran</span></a></li>
      <li>
         <a href="javascript: void(0);"><i class="mdi mdi-chart-line"></i><span>Laporan</span><span class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>

         <ul class="nav-second-level" aria-expanded="false">
            <li><a href="{{ URL::to('kasir/laporanpemesanan') }}">Pesanan</a></li>
            <li><a href="{{ URL::to('kasir/laporanpembayaran') }}">Pembayaran</a></li>
         </ul>

      </li>
      
   </ul>
</div>