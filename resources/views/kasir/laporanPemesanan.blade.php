@extends('kasir.layout.mainLayout')

@section('pageCss')

<link href="{{ asset('admin/assets/plugins/sweet-alert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('admin/assets/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('admin/assets/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('admin/assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css">

@endsection
@section('content')
	<div class="container-fluid">
       <!-- Page-Title -->
       <div class="row">
          <div class="col-sm-12">
             <div class="page-title-box">
                <div class="float-right">
                   <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="javascript:void(0);">Kasir</a></li>
                      <li class="breadcrumb-item active">Laporan Pemesanan</li>
                   </ol>
                </div>
                <h4 class="page-title">Laporan Pemesanan</h4>
             </div>
          </div>
       </div>
       <!-- end page title end breadcrumb -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-body">
                <div class="row">
                <h4 class="mt-0 col-md-12">Laporan Pemesanan
                        
                </h4>
                
                  <div class="col-md-12">
                    
                    <table class="table mb-0 dataTable" id="table1">
                      <thead>
                        <tr>
                          <th>#No_pesanan</th>
                          <th>Nama Pemesan</th>
                          <th>No. Meja</th>
                          <th>Status Pesanan</th>
                          <th>Total</th>                          
                        </tr>
                      </thead>
                      <tbody>  
                        @foreach($pemesanan as $p)                      
                          <tr>
                            <td>#{{ $p->no_pemesanan }}</td>
                            <td>{{ $p->nama_pesanan }}</td>
                            <td>{{ $p->no_meja }}</td>
                            <td>{{ $p->status }}</td>
                            <th>{{ $p->total_harga }}</th>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>

				
		


        

 	</div>
@endsection

@section('scriptPage')

<script src="{{ asset('admin/assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/assets/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{ asset('admin/assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('admin/assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('admin/assets/pages/jquery.table-datatable.js') }}"></script>
<script src="{{ asset('admin/assets/plugins/sweet-alert2/sweetalert2.min.js') }}"></script>
<script src="{{ asset('admin/assets/pages/jquery.sweet-alert.init.js') }}"></script>

<script type="text/javascript">
  
  $(document).ready(function() {
    $('#table1').DataTable();      

  });

</script>
	  
@endsection

