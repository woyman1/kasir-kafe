@extends('kasir.layout.mainLayout')

@section('pageCss')
@endsection
@section('content')
	<div class="container-fluid">
       <!-- Page-Title -->
       <div class="row">
          <div class="col-sm-12">
             <div class="page-title-box">
                <div class="float-right">
                   <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="javascript:void(0);">Pembayaran</a></li>
                      <li class="breadcrumb-item active"></li>
                   </ol>
                </div>
                <h4 class="page-title"></h4>
             </div>
          </div>
       </div>
       <!-- end page title end breadcrumb -->

       <div class="row">
         <div class=" container col-md-8">
            <div class="card">
              <div class="card-header">
                <h3 class="text-center">#{{ $pembayaran[0]->no_pesanan }}</h3>                
              </div>
              <div class="card-body">

                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-2 col-form-label">Nama Pemesan</label>
                    <div class="col-sm-10">
                      <input type="text" value="{{ $pembayaran[0]->nama_pesanan }}" class="form-control" readonly>
                    </div>
                  </div>

                <table class="table mb-0 dataTable" id="table1">
                      <thead>
                        <tr>
                          <th>Nama Menu</th>
                          <th>@</th>
                          <th>Jumlah</th>
                          <th>Total</th>
                        </tr>
                      </thead>
                      <tbody>
                          @foreach($detail as $d)
                            <tr>
                              <td>{{ $d->nama_menu }}</td>
                              <td>{{ $d->harga }}</td>
                              <td>{{ $d->jumlah }}</td>
                              <th>{{ $d->total_detail }}</th>
                            </tr>
                          @endforeach
                      </tbody>
                      <tbody>
                            <tr>
                              <th colspan="3">Total </th>
                              <th id="harga_total">{{ $pembayaran[0]->total_harga }}</th>
                            </tr>
                      </tbody>
                </table>
                <hr>
              <form action="{{ URL::to('kasir/simpanpembayaran') }}" method="post">
                @csrf
                <input type="hidden" name="id_pembayaran" value="{{ $pembayaran[0]->id_pembayaran }}">
                <div class="form-group row">
                  <label for="example-text-input" class="col-sm-2 col-form-label">Tunai </label>
                  <div class="col-sm-10">
                    <input type="text" value="" name="tunai" id="tunai" class="form-control" required>
                  </div>
                </div>

                <div class="form-group row">
                  <label for="example-text-input" class="col-sm-2 col-form-label">Kembali </label>
                  <div class="col-sm-10">
                    <input type="text" value="" name="kembalian" id="kembalian" class="form-control" readonly>
                  </div>
                </div>

                <div class="form-group row">                  
                    <button type="submit" class="btn btn-info btn-lg col-md-11 container"> Simpan </button>                  
                </div>
            </form>

                
              </div>
            </div>
          </div>
      </div>
		
 	</div>
@endsection

@section('scriptPage')
<script type="text/javascript">
  
  $(document).ready(function() {
    
    $('#tunai').keyup(function(event) {

      var tunai = parseInt($(this).val());

      var harga_total = parseInt($('#harga_total').html());

      var kembali = String(tunai - harga_total);

      if(tunai > harga_total )
      {
        $('#kembalian').val(kembali);  
      }else{
        $('#kembalian').val('0');  
      }

    });

  });

</script>
	  
@endsection

