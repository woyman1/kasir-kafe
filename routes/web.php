<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('auth.login');
});

Route::group(['prefix' => 'pelayan'], function () {
	Route::group(['namespace' => 'pelayan'], function () {

		Auth::routes();	
		
		Route::post('/sendlogin','\App\Http\Controllers\pelayan\Auth\LoginController@login')->name('pelayanLogin');
		Route::post('/logout','\App\Http\Controllers\pelayan\Auth\LoginController@logout')->name('pelayanLogout');
		Route::get('/logout','\App\Http\Controllers\pelayan\Auth\LoginController@logout');

		Route::group(['middleware' => 'auth'], function () {	
			
			Route::get('/', 'PelayanController@backtoindex');
			Route::get('/home', 'PelayanController@index');
			Route::get('/daftarpesanan', 'PesananController@index');
			Route::get('/tambahpesanan', 'PesananController@tambahpesanan');
			Route::post('/simpanpesanan', 'PesananController@simpanpesanan');
			Route::post('/carimenu', 'PesananController@carimenu');
			Route::get('/carimenu', 'PesananController@getMenu');
			Route::get('/updatepesanan/{id}', 'PesananController@formUpdate');
			Route::post('/simpanupdatepesanan', 'PesananController@simpanUpdate');
			Route::get('/hapuspesanan', 'PesananController@hapusPesanan');

			Route::get('/laporanpemesanan', 'PesananController@laporan');			

		 });
	});
});

Route::group(['prefix' => 'kasir'], function () {
	Route::group(['namespace' => 'kasir'], function () {

		Auth::routes();	
		// Route::get('/login','\App\Http\Controllers\kasir\Auth\LoginController@showLoginForm')->name('formLoginKasir');		
		Route::get('/login_kasir','\App\Http\Controllers\kasir\Auth\LoginController@showLoginForm')->name('formLoginKasir');		
		Route::get('/logout','\App\Http\Controllers\kasir\Auth\LoginController@logout');


		Route::group(['middleware' => 'auth:kasir'], function () {

			Route::get('/', 'KasirController@backtoindex');
			Route::get('/home', 'KasirController@index');

			Route::get('/daftarpesanan', 'PesananController@index');
			Route::get('/tambahpesanan', 'PesananController@tambahpesanan');
			Route::post('/simpanpesanan', 'PesananController@simpanpesanan');
			Route::post('/carimenu', 'PesananController@carimenu');
			Route::get('/carimenu', 'PesananController@getMenu');
			Route::get('/updatepesanan/{id}', 'PesananController@formUpdate');
			Route::post('/simpanupdatepesanan', 'PesananController@simpanUpdate');
			Route::get('/hapuspesanan', 'PesananController@hapusPesanan');

			Route::get('/listpembayaran', 'PembayaranController@index');
			Route::get('/pembayaran/{id}', 'PembayaranController@bayar');			
			Route::post('/simpanpembayaran', 'PembayaranController@simpanBayar');
			
			Route::get('/laporanpemesanan', 'PesananController@laporan');			
			Route::get('/laporanpembayaran', 'PembayaranController@laporan');			


		});
	});
});




// Route::group(['prefix' => 'admin'], function () {

// 	Route::group(['middleware' => 'auth'], function () { 

// 		Route::get('/home', 'HomeController@index')->name('home');
// 		Route::get('/tambahwisata', 'WisataController@formTambahWisata');
// 		Route::get('/daftarwisata', 'WisataController@index');
// 		Route::post('/inputwisata', 'WisataController@prosesInputWisata')->name('inputwisata');
// 		Route::get('/viewWisata/{id}', 'WisataController@viewWisata')->name('reviewWisata');
// 		Route::get('/editwisata/{id}', 'WisataController@formEditWisata');
// 		Route::post('/simpaneditwisata', 'WisataController@editWisata')->name('simpanEditWisata');
// 		Route::get('/hapuswisata/{id}', 'WisataController@deleteWisata');

// 	});
// });

