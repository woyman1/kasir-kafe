<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMenumakanan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_makanan', function (Blueprint $table) {
            $table->bigIncrements('id_menu');
            $table->string('nama_menu');
            $table->enum('jenis_menu', ['makanan', 'minuman']);
            $table->integer('harga');
            $table->enum('ready', ['ready', 'not ready']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_makanan');
    }
}
