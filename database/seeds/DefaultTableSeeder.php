<?php

use Illuminate\Database\Seeder;

class DefaultTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         \App\User::insert([
            [ 
              'email_karyawan'  	   		=> 'pelayan@gmail.com',
              'nama_karyawan'			    => 'pelayan',
              'password'  		=>  bcrypt('pelayan123'),
              'level_karyawan'        => 'pelayan',
              'nohp_karyawan'         => '081330222892',
              'created_at'            => \Carbon\Carbon::now('Asia/Jakarta')
            ],
            [
              'email_karyawan'        => 'kasir@gmail.com',
              'nama_karyawan'         => 'kasir',
              'password'     =>  bcrypt('kasir123'),
              'level_karyawan'        => 'kasir',
              'nohp_karyawan'         => '081330253582',
              'created_at'            => \Carbon\Carbon::now('Asia/Jakarta') 
            ]
        ]);

       for ($i=1; $i < 21 ; $i++) { 
          
          \App\No_meja::insert([
              'no_meja' => $i,
              'created_at'  => \Carbon\Carbon::now('Asia/Jakarta')
          ]);

       }


    }
}
