<?php

use Illuminate\Database\Seeder;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         \App\Menu_makanan::insert([
            [ 
              'nama_menu'  	   		=> 'Gado-Gado',
              'jenis_menu'        => 'makanan',
              'harga'             => 15000,
              'ready'             => 'ready',
              'created_at'        => \Carbon\Carbon::now('Asia/Jakarta') 
            ],
            [ 
              'nama_menu'         => 'Rujak',
              'jenis_menu'        => 'makanan',
              'harga'             => 8000,
              'ready'             => 'ready',
              'created_at'        => \Carbon\Carbon::now('Asia/Jakarta') 
            ],
            [ 
              'nama_menu'         => 'Sate kambing',
              'jenis_menu'        => 'makanan',
              'harga'             => 25000,
              'ready'             => 'not ready',
              'created_at'        => \Carbon\Carbon::now('Asia/Jakarta') 
            ], 
            [ 
              'nama_menu'         => 'Es Teh',
              'jenis_menu'        => 'minuman',
              'harga'             => 3000,
              'ready'             => 'ready',
              'created_at'        => \Carbon\Carbon::now('Asia/Jakarta') 
            ],
            [ 
              'nama_menu'         => 'Es Jeruk',
              'jenis_menu'        => 'minuman',
              'harga'             => 3500,
              'ready'             => 'ready',
              'created_at'        => \Carbon\Carbon::now('Asia/Jakarta') 
            ],
            [ 
              'nama_menu'         => 'Jus Alpukat',
              'jenis_menu'        => 'minuman',
              'harga'             => 10000,
              'ready'             => 'not ready',
              'created_at'        => \Carbon\Carbon::now('Asia/Jakarta') 
            ]
        ]);

      


    }
}
